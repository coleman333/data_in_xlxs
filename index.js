const path = require('path');
const XLSX = require('xlsx');

const fileName = path.join(__dirname, './test.xlsx');
let workBook = XLSX.readFile(fileName);
const ws = workBook.Sheets['Sheet1'];

const data = XLSX.utils.sheet_to_json(ws);

console.log(`\n=>`,data);

data.push({name: 'alex3', age: 10});
data.push({name: 'yegor', age: 20});

XLSX.utils.sheet_add_json(ws, data);

XLSX.writeFile(workBook, fileName);